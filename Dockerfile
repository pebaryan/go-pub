FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /go-pub
WORKDIR /go-pub
ADD requirements.txt /go-pub/
RUN pip install -r requirements.txt
ADD . /go-pub/