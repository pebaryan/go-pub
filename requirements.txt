Django>=2.2.4
psycopg2
django-webpack-loader==0.6.0
djangorestframework==3.10.2
django-cleanup>=4.0.0