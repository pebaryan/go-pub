from django.apps import AppConfig


class PubmatchConfig(AppConfig):
    name = 'pubmatch'
