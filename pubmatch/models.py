from django.db import models


class Article(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=250)
    abstract = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('created',)


class Document(models.Model):
    article_id = models.ForeignKey(Article,
                                   related_name='files',
                                   on_delete=models.CASCADE)
    content = models.FileField(upload_to='documents/')
    uploaded = models.DateTimeField(auto_now_add=True)
