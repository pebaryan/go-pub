from rest_framework import serializers
from pubmatch.models import *


class ArticleSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=250)
    abstract = serializers.CharField()

    def create(self, validated_data):
        return Article.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.abstract = validated_data.get('abstract', instance.title)
        instance.save()
        return instance
