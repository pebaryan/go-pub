from django.conf.urls import url
from pubmatch import views

urlpatterns = [
    url(r'^articles/$', views.list_articles),    
]